//
//  FlashcardSet.swift
//  flashcards
//
//  Created by Sean Denney on 1/23/22.
//  Copyright © 2022 Sean Denney. All rights reserved.
//

import Foundation

class FlashcardSet {
    
    var title: String
    
    init(title: String) {
        self.title = title
    }
    
    func tenFlashcardSet() -> Array<FlashcardSet> {
        
        let flashSet00 = FlashcardSet(title: "TITLE")
        let flashSet01 = FlashcardSet(title: "TITLE")
        let flashSet02 = FlashcardSet(title: "TITLE")
        let flashSet03 = FlashcardSet(title: "TITLE")
        let flashSet04 = FlashcardSet(title: "TITLE")
        let flashSet05 = FlashcardSet(title: "TITLE")
        let flashSet06 = FlashcardSet(title: "TITLE")
        let flashSet07 = FlashcardSet(title: "TITLE")
        let flashSet08 = FlashcardSet(title: "TITLE")
        let flashSet09 = FlashcardSet(title: "TITLE")
        
        let arrOfSet = [flashSet00, flashSet01, flashSet02, flashSet03, flashSet04, flashSet05, flashSet06,flashSet07, flashSet08, flashSet09]
        
        return arrOfSet
        
    }
    
}
