//
//  Flashcard.swift
//  flashcards
//
//  Created by Sean Denney on 1/23/22.
//  Copyright © 2022 Sean Denney. All rights reserved.
//

import Foundation

class Flashcard {
    
    var term: String
    var definition: String
    
    init(term: String, definition: String) {
        self.term = term
        self.definition = definition
    }
    
    func tenFlashcards() -> Array<Flashcard> {
        
        let flash00 = Flashcard(term: "TERM", definition: "DEF")
        let flash01 = Flashcard(term: "TERM", definition: "DEF")
        let flash02 = Flashcard(term: "TERM", definition: "DEF")
        let flash03 = Flashcard(term: "TERM", definition: "DEF")
        let flash04 = Flashcard(term: "TERM", definition: "DEF")
        let flash05 = Flashcard(term: "TERM", definition: "DEF")
        let flash06 = Flashcard(term: "TERM", definition: "DEF")
        let flash07 = Flashcard(term: "TERM", definition: "DEF")
        let flash08 = Flashcard(term: "TERM", definition: "DEF")
        let flash09 = Flashcard(term: "TERM", definition: "DEF")
        
        
        let arrOfFlash = [flash00, flash01, flash02, flash03, flash04, flash05, flash06, flash07, flash08, flash09]
        
        return arrOfFlash
        
//        return flash00 = Flashcard("TERM", "DEF")
        
    }
    
    
}
