//
//  ViewController.swift
//  flashcards
//
//  Created by Sean Denney on 1/23/22.
//  Copyright © 2022 Sean Denney. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print("THIS IS A TEST FOR 'flashcard'")
        
        let testFlash = Flashcard(term: "TESTterm", definition: "TESTdef")
        print(testFlash)
        print(testFlash.tenFlashcards())
        
        print("THIS IS A TEST FOR 'flashcardset'")
        
        let testFlashSet = FlashcardSet(title: "TESTtitle")
        print(testFlashSet)
        print(testFlash.tenFlashcards())
        
    }
	

}

